#!/usr/bin/env python
# -*- coding: utf-8 -*-  

import time
import random
import subprocess

class Fortune(object):
	"""docstring for Fortune"""

	jars=["bofh-excuses", "de", "off", "debian-hints", "linux", "debian", "kids", "linuxcookie", "men-women", "news", "translate-me", "startrek"]
	DEFAULT_PAUSE=3600
	DEFAULT_LENGTH=1024
	length=1024
	nextFortune=0
	pause=3600

	def __init__(self):
		super(Fortune, self).__init__()
		self.nextFortune=time.time()-1

	def getFortune(self,force=False,jars=jars):
		if time.time()>self.nextFortune or force:
			fortune=self.runFortune(["-s","-n",str(self.length)]+jars)
			if not force:
				self.delay()
			return fortune

	def getDistribution(self):
		return self.runFortune(self.jars+["-f"])

	def getLongFortune(self):
		return self.runFortune(self.jars+["-l","-n",str(self.length)])

	def getOffFortune(self):
		#return self.runFortune(["off"])
		return self.getFortuneAndNarrator(True, ["off"])

	def runFortune(self,parameters):
		return subprocess.check_output(["fortune"]+parameters, stderr=subprocess.STDOUT)

	def setLength(self,length):
		if length>20 and length<2155:
			self.length=length;
			return True
		else:
			return False

	def resetLength(self):
		self.length=self.DEFAULT_LENGTH

	def delay(self):
		self.nextFortune+=self.pause+(15*random.random())

	def resetTimer(self):
		self.nextFortune=time.time()
		#self.pause=self.DEFAULT_PAUSE

	def slowDown(self):
		self.pause*=2
		self.resetTimer()

	def speedUp(self):
		self.pause/=2
		self.resetTimer()

	def getNextTime(self):
		return self.nextFortune

	def getFortuneAndNarrator(self,forced=False,jars=jars):
		fortune=self.getFortune(forced,jars)
		if fortune is None:
			return None
		lines=fortune.split("\n")
		lastLine=lines[lines.__len__()-2]
		narrator=None
		print "check narrator"
		if self.isNarrator(lastLine):
			print "--narrator"
			tmp=lastLine.split("--")
			tmp=tmp[1].strip().split("(")
			tmp=tmp[0].strip().split(",")
			print "done"
			narrator=tmp[0].strip()
			return ["\n".join(lines[0:-2]),narrator,lastLine]
		elif self.isOtherNarrator(lastLine):
			print "other narrator"
			tmp=lastLine.strip().split("[")
			tmp=tmp[1].strip().split("]")
			tmp=tmp[0].strip().split(",")
			print "done"
			narrator=tmp[0].strip()
			return ["\n".join(lines[0:-2]),narrator,lastLine]
		print "no narrator"
		return[fortune,None,'']

	def isNarrator(self,line):
		return line.find("\t--") != -1 
	def isOtherNarrator(self,line):
		return line.find("[") !=-1 #and line.find("]") !=-1)
		#return False