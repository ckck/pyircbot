#!/usr/bin/env python
# -*- coding: utf-8 -*- 


#TODO:
# * beschimpfen für off-zitate
# * mehrzeilige quellangaben (off)
import socket
import time
import subprocess
import os
import re
from fortune import Fortune

serverip = '192.168.2.34'
port = 6667
serverPassword= 'lerngruppeCCJ'

channel='#wtf'
nick='fortune'
newNick=None
remote=False
#TELL=["tell","hey", "hallo", "hi"]

if os.path.exists("dev"):
	channel='#fortune'	#dev
	nick='fortune2'		#dev
	print "DEVMODE"
if os.path.exists("remote"):
	channel='#fortune'
	nick='fortuneRemote'
	serverip="irc.agp8x.org"
	port=6666
	remote = True
	print "REMOTE DEVMODE"

firstNick=nick

def sendMessage(message,target=channel):
	if message is None:
		return
	lines=message.split("\n")
	for line in lines:
		if target==channel:
			notifyChannel(line)
		else:
			notifyUser(target, line)

def printStatus():
	tmp=subprocess.check_output("uptime")
	tmp2=""
	try:
		tmp2=os.popen('/opt/vc/bin/vcgencmd measure_temp').readline().strip()
	except Exception :
		pass
	sendMessage(tmp+tmp2+'\r\n')

def findOrder(key, data):
	return data.find(nick)!=-1 and data.find(key)!=-1

#def findAnyOrder(keys,data):
#TODO: List.contains: "x" in list
#	if data.find(nick)!=-1:
#		for key in keys:
#			if data.find(key):
#				return True
#	return False


def getNameFromMessage(data):
	part=data.split("!")[0]
	name=part.split(":")[1]
	return name

def getParameter(data):
	return data.split(":")[2]


def notifyChannel(message):
	irc.send('PRIVMSG '+channel+' :'+message+'\r\n')

def notifyUser(user,message):
	irc.send('PRIVMSG '+user+' :'+message+'\r\n')

def findText(data, text):
	return data.find(text)!=-1

def rename(data):
	tmp=data.split("!")
	#tmp2=tmp[2].split(",")
	changeName(tmp[2])
def changeName(name):
	global newNick
	name=name.strip()
	name=name.replace(" ","_")
	name=name.replace(".","")
	newNick=name
	irc.send('NICK :'+newNick+'\r\n')
def rename2(data):
	global newNick,nick
	success=(data.find(":"+newNick)!=-1)
	if success:
		#sendMessage("new name:"+str(newNick))
		nick=newNick.strip()
		newNick=None
	else:
		sendMessage("new nick '"+newNick+"' rejected")
		newNick=None
def sendQuote(quote):
	if not quote is None:
		oldNick=nick
		if not quote[1] is None:
			changeName(quote[1])
		sendMessage(quote[0])
		if not quote[1] is None:
			changeName(oldNick)
			sendMessage(quote[2])

fortune=Fortune()
irc = socket.socket ( socket.AF_INET, socket.SOCK_STREAM )
irc.connect ( ( serverip, port ) )
print irc.recv ( 4096 )
if remote:
	print "using password"
	irc.send('PASS '+serverPassword+'\r\n')
irc.send ( 'NICK '+nick+'\r\n' )
irc.send ( 'USER '+nick+' '+nick+' '+nick+' :Python IRC\r\n' )
irc.send ( 'JOIN '+channel+'\r\n' )
sendMessage("ich bringe euch Spass!")
irc.settimeout(10)


while True:
	quote=fortune.getFortuneAndNarrator()
	sendQuote(quote)

	try:
		data=irc.recv(4096)
		## protocol
		if data.find ( 'PING' ) != -1:
			irc.send ( 'PONG ' + data.split() [ 1 ] + '\r\n' )
		## commands
		if findOrder("hey",data) :
			quote=fortune.getFortuneAndNarrator(True)
			sendQuote(quote)
		if findOrder("status",data):
			printStatus()
		if findOrder("delay",data):
			fortune.delay()
			next=time.localtime(fortune.getNextTime())
			sendMessage('delayed to '+str(next[3])+'h')
		if findOrder("spoil",data):
			next=time.lstrftime("%H:%M",fortune.getNextTime())
			notifyUser(getNameFromMessage(data),'next fortune scheduled for '+next)
			sendMessage(getNameFromMessage(data)+' was curious to know next fortune')
		if findOrder("reset",data):
			fortune.resetTimer()
			sendMessage('Thank you, '+getNameFromMessage(data)+', you are a kind man')
		if findOrder("help",data):
			sendMessage("Possible commands: help; hey; status; delay; spoil; reset; slower; faster; jars; off")
		if findOrder("slower", data):
			fortune.slowDown()
			sendMessage("slowing down")
		if findOrder("faster", data):
			fortune.speedUp()
			sendMessage("speeding up")
		if findOrder("jars", data):
			sendMessage(fortune.getDistribution())
		if findOrder("off", data):
			sendQuote(fortune.getOffFortune())
		if findOrder("name", data):
			rename(data)
		if findOrder(nick+"!"+firstNick+"@", data):
			rename2(data)
		if findOrder("nick", data):
			sendMessage("You can call me '"+nick+"'")
		if findOrder("length", data):
			fortune.setLength(int(re.sub("\D", "", data)))
		## awareness
		if findText(data, "gut nacht"):
			sendMessage("Gute Nacht, "+getNameFromMessage(data))
		print data
		if(data==""):
			break
	except Exception, e:
		print e
	except KeyboardInterrupt :
		break
sendMessage("ByeBye suckers!")
